#!/usr/bin/env node
import { SsdpControlPoint, SsdpControlPointEvents, SsdpSocket } from 'dotup-ts-node-ssdp';
// tslint:disable-next-line: match-default-export-name
import { configure, getLogger } from 'log4js';
import { InterfaceSelector } from './InterfaceSelector.js';

// logger
const mode = process.env.NODE_ENV === undefined ? 'production' : process.env.NODE_ENV;
const config = configure(`${__dirname}/assets/logging.${mode}.json`);
const logger = getLogger('SSDP');

class SsdpControlPointCli {
  async start(): Promise<void> {
    logger.info(`Dotup device discovery service control point.`);

    const iface = await InterfaceSelector.getInterfaces();

    // Create socket
    const sock = new SsdpSocket(iface);
    sock.initialize();

    // Create control point
    const controlPoint = new SsdpControlPoint(sock); // , 10, 'vEthernet (hyperv-extern-wlan)');

    controlPoint.on(SsdpControlPointEvents.found, (item, message) => {
      logger.info(`Found: ${message.USN}`);
    });

    controlPoint.on(SsdpControlPointEvents.lost, (item) => {
      logger.info(`Lost: ${item.USN}`);
    });

    controlPoint.initialize();

    process.on('SIGINT', async () => {
      controlPoint.removeAllListeners();
      controlPoint.dispose();
      sock.dispose();
    });

  }

}

const app = new SsdpControlPointCli();
app
  .start()
  .catch(e => console.error(e))
  ;
