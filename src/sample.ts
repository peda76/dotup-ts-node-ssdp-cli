import { SsdpAdvertiser, SsdpControlPoint, SsdpControlPointEvents, SsdpSocket, SsdpSocketEvents } from 'dotup-ts-node-ssdp';
import { configure } from 'log4js';
import cliConfigJson from './assets/cli.config.json';

class Sample {
  los() {

    const config = configure(`${__dirname}/../assets/logging.${process.env.NODE_ENV}.json`);

    // Create socket
    const sock = new SsdpSocket('vEthernet (hyperv-extern-wlan)');
    sock.initialize();

    // Create advertiser
    const advertiser = new SsdpAdvertiser(sock, cliConfigJson.Ssdp); // , 10, 'vEthernet (hyperv-extern-wlan)');
    advertiser.startPublishing();

    // Create control point
    const controlPoint = new SsdpControlPoint(sock);
    controlPoint.initialize();

    controlPoint.on(SsdpControlPointEvents.found, (item, message) => {
      console.log(item);
    });
    controlPoint.on(SsdpControlPointEvents.lost, (item) => {
      console.log(item);
    });
    setTimeout(
      async () => {
        await advertiser.stopPublishing();
      },
      30000
    );

    sock.on(SsdpSocketEvents.msearch, msg => {
      console.log(msg);
    });

  }

}

const sample = new Sample();
sample.los();
