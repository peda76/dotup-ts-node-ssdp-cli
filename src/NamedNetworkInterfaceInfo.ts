import * as os from 'os';

// tslint:disable-next-line: interface-name
export interface NamedNetworkInterfaceInfo {
  name: string;
  netInfo: os.NetworkInterfaceInfo;
}
