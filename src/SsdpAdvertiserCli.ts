#!/usr/bin/env node
import { SsdpAdvertiser, SsdpSocket } from 'dotup-ts-node-ssdp';
// tslint:disable-next-line: match-default-export-name
import { configure, getLogger } from 'log4js';
import cliConfigJson from './assets/cli.config.json';
import { InterfaceSelector } from './InterfaceSelector.js';

// logger
const mode = process.env.NODE_ENV === undefined ? 'production' : process.env.NODE_ENV;
const config = configure(`${__dirname}/assets/logging.${mode}.json`);
const logger = getLogger('SSDP');

class SsdpAdvertiserCli {
  async start(): Promise<void> {
    logger.info(`Dotup device discovery service advertiser.`);

    const iface = await InterfaceSelector.getInterfaces();

    // Create socket
    const sock = new SsdpSocket(iface);
    sock.initialize();

    // Create advertiser
    const advertiser = new SsdpAdvertiser(sock, cliConfigJson.Ssdp); // , 10, 'vEthernet (hyperv-extern-wlan)');
    advertiser.startPublishing();

    process.on('SIGINT', async () => {
      await advertiser.dispose();
      sock.dispose();
    });

  }

}

const app = new SsdpAdvertiserCli();
app
  .start()
  .catch(e => console.error(e))
  ;
