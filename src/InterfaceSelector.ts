// tslint:disable-next-line: match-default-export-name
import enquirer from 'enquirer';
import * as os from 'os';
import { NamedNetworkInterfaceInfo } from './NamedNetworkInterfaceInfo';

export namespace InterfaceSelector {

  export async function getInterfaces(): Promise<string> {
    const interfaces = os.networkInterfaces();
    const info: NamedNetworkInterfaceInfo[] = [];
    const keys = Object.keys(interfaces);

    keys.forEach(key => {
      const iface = interfaces[key];
      iface.forEach(item => {
        if (item.family === 'IPv4' && !item.internal) {
          info.push({ name: key, netInfo: item });
        }
      });
    });

    const choices = info.map(device => {
      return {
        name: `${device.name} - (${device.netInfo.address})`,
        value: device
      };
    });

    if (choices.length < 1) {
      console.log('Could not start without network interface.');
      process.exit(1);

      return;
    }

    const answer = await enquirer.prompt<{ device: NamedNetworkInterfaceInfo }>({
      type: 'select',
      name: 'device',
      message: 'Choose network interface',
      choices: choices,
      result: (selection) => {
        const v = choices.find(x => x.name === selection);

        // tslint:disable-next-line: no-any
        return <any>v.value;
      }
    });

    return answer.device.name;
  }
}
